import React, {ReactElement} from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useHistory
} from "react-router-dom";
import './App.css';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import History from "./features/history/History";
import Grid from '@material-ui/core/Grid';
import {Counter} from "./features/counter/Counter";
import Single from "./features/single/Single";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    header: {
      'text-align': 'center',
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
      display: 'inline',
    },
    root: {
      flexGrow: 1,
    },
    datePicker: {
      'margin-left': theme.spacing(1),
    },
    loading: {
      display: 'flex',
      justifyContent: 'center',
    }
  }),
);

function NavBar() {
  const history = useHistory();

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    history.push(event.currentTarget.name);
  };

  return (
    <div>
      <button type="button" name="for-day" onClick={handleClick}>
        Recent
      </button>
      <button type="button" name="last-n-days" onClick={handleClick}>
        History (Last 7 days)
      </button>
      <button type="button" name="redux-counter" onClick={handleClick}>
        Redux Counter Tutorial
      </button>
    </div>
  );
}

export default App;

function App(): ReactElement {
  const classes = useStyles();

  return (
    <Router>
      <div>
        <header className={classes.header}>
          <h1>Exchange Rates</h1>
        </header>
        <main>
          <Grid container className={classes.root} spacing={2}>
            <Grid item xs={12}>
              <Grid container justify="center">
                <Grid item sm={12} md={6}>
                  <NavBar/>
                  <Switch>
                    <Route exact path="/">
                      <div>Homepage</div>
                    </Route>
                    <Route path="/for-day">
                      <Single></Single>
                    </Route>
                    <Route path="/last-n-days">
                      <History></History>
                    </Route>
                    <Route path="/redux-counter">
                      <Counter></Counter>
                    </Route>
                    <Route path="*">
                      <div>404 NOT FOUND</div>
                    </Route>
                  </Switch>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </main>
        <footer>
          <hr/>
          <small>- ezyVet React Project</small>
        </footer>
      </div>
    </Router>
  );
}
