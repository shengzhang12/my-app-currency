import { configureStore } from "@reduxjs/toolkit";
import counterReducer from "../features/counter/counterSlice";
import singleReducer from "../features/single/singleSlice";
import historyReducer from "../features/history/historySlice"

export default configureStore({
  reducer: {
    counter: counterReducer,
    single: singleReducer,
    history: historyReducer,
  },
})