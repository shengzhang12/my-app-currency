import React from 'react';
import {
  render,
  fireEvent,
  waitForElement,
  getByText,
} from '@testing-library/react';
import App from './App';

test('renders nzd as default currency', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/New Zealand Dollar/i);
  expect(linkElement).toBeInTheDocument();
});

const DOWN_ARROW = { keyCode: 40 };

it('renders and values can be filled then submitted', async () => {
  const {
    asFragment,
    getByLabelText,
    getByText,
  } = render(<App />);

  // the function
  const getSelectItem = (getByLabelText, getByText) => async (selectLabel, itemText) => {
    fireEvent.keyDown(getByLabelText(selectLabel), DOWN_ARROW);
    await waitForElement(() => getByText(itemText));
    fireEvent.click(getByText(itemText));
  }

  // usage
  const selectItem = getSelectItem(getByLabelText, getByText);

  await selectItem('currency', 'United States Dollar');

  const usdOption = getByText(/United States Dollar/i);
  expect(usdOption).toBeInTheDocument();
});