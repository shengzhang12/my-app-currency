import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import moment from "moment";
import {client} from '../../api/client'
import {Rate} from "../../interfaces/app"

export const fetchLatest =  createAsyncThunk(
  'single/fetchLatest',
  async (base: string) => {
    const response = await client.get(`https://api.exchangeratesapi.io/latest?base=${base}`)
    return response.rates
  }
)

const slice = createSlice({
  name: "single",
  initialState: {
    currency: 'NZD' as string,
    date: moment().format('YYYY-MM-DD') as string,
    rates: [] as Array<Rate>,
  },
  reducers: {
    setCurrency: (state, action) => {
      state.currency = action.payload
    },
    setDate: (state, action) => {
      state.date = action.payload
    },
  },
  extraReducers: {
    [fetchLatest.fulfilled.toString()]: (state, action) => {
      state.rates = Object.entries<number>(action.payload).map( ([code, rate]): Rate => {
        return {
          symbolCode: code,
          rate: rate,
        } as Rate
      })
    },
    [fetchLatest.rejected.toString()]: (state, action) => {
      console.log(action.error.message)
    }
  }
})

export const {setCurrency, setDate} = slice.actions

export const selectCurrency = (state: any): string => state.single.currency
export const selectDate = (state: any): string => state.single.date
export const selectRates = (state: any): Array<Rate> => state.single.rates

export default slice.reducer