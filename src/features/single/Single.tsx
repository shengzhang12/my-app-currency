import React, {ReactElement, useEffect} from "react";
import FormControl from "@material-ui/core/FormControl";
import NativeSelect from "@material-ui/core/NativeSelect";
import {fetchLatest, selectCurrency, selectDate, selectRates, setCurrency, setDate} from "./singleSlice";
import TextField from "@material-ui/core/TextField";
import CircularProgress from "@material-ui/core/CircularProgress";
import SingleTable from "./SingleTable";
import {useDispatch, useSelector} from "react-redux";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {currencies} from "../../helpers/data";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    header: {
      'text-align': 'center',
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
      display: 'inline',
    },
    root: {
      flexGrow: 1,
    },
    datePicker: {
      'margin-left': theme.spacing(1),
    },
    loading: {
      display: 'flex',
      justifyContent: 'center',
    }
  }),
);

export default function Single(): ReactElement {
  const [isLoading, setIsLoading ] = React.useState<boolean>(false);
  const classes = useStyles();
  const currencyOptions = currencies.map(
    currency => <option value={currency.code} key={currency.code}>{currency.name}</option>
  );

  const currency = useSelector(selectCurrency)
  const date = useSelector(selectDate)
  const rates = useSelector(selectRates)
  const dispatch = useDispatch()

  useEffect( () => {
    console.log('dispatch')
    dispatch(fetchLatest(currency))
  }, [date, currency]);

  return (
    <div>
      <FormControl className={classes.formControl}>
        <NativeSelect
          value={currency}
          onChange={e => dispatch(setCurrency(e.target.value))}
          name="currency"
          inputProps={{'aria-label': 'currency'}}
        >
          {currencyOptions}
        </NativeSelect>
        <TextField
          id="date"
          type="date"
          onChange={e => dispatch(setDate(e.target.value))}
          name="date"
          value={date}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            max: date
          }}
          className={classes.datePicker}
        />
      </FormControl>
      { isLoading
        ? <div className={classes.loading}>
          <CircularProgress/>
        </div>
        : <SingleTable rows={rates}/>
      }
    </div>
  );
}