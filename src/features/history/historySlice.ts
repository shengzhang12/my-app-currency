import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import {client} from "../../api/client";

export const fetchHistory =  createAsyncThunk(
  'history/fetchHistory',
  async ({startDate, endDate, baseCode, symbolCode}: {startDate: string, endDate: string, baseCode: string, symbolCode: string}) => {
    const response = await client.get(`https://api.exchangeratesapi.io/history?start_at=${startDate}&end_at=${endDate}&base=${baseCode}&symbols=${symbolCode}`)
    return response.rates
  }
)

export const slice = createSlice({
  name: "history",
  initialState: {
    baseCode: 'NZD' as string,
    symbolCode: 'USD' as string,
    rows: [] as Array<{date: string, rate: number}>,
  },
  reducers: {
    setBaseCode: (state, action) => {
      state.baseCode = action.payload
    },
    setSymbolCode: (state, action) => {
      state.symbolCode = action.payload
    },
  },
  extraReducers: {
    [fetchHistory.fulfilled.toString()]: (state, action) => {
      console.log(action.payload)
      state.rows = Object.entries<{[key: string]: number}>(action.payload).map(([date, rate]) => {
        console.log(rate);
        return {
          date: date as string,
          rate: rate[state.symbolCode] as number,
        }
      });
    },
    [fetchHistory.rejected.toString()]: (state, action) => {
      console.log(action.error.message)
    }
  }
})

export const {setBaseCode, setSymbolCode} = slice.actions

export const selectBaseCode = (state: any): string => state.history.baseCode
export const selectSymbolCode = (state: any): string => state.history.symbolCode
export const selectRows = (state: any): Array<any> => state.history.rows

export default slice.reducer