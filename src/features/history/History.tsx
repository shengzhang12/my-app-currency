import React, {ReactElement, useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import FormControl from "@material-ui/core/FormControl";
import NativeSelect from "@material-ui/core/NativeSelect";
import InputLabel from "@material-ui/core/InputLabel";
import moment from 'moment';
import {useSelector, useDispatch} from "react-redux";
import {
  setBaseCode,
  setSymbolCode,
  selectBaseCode,
  selectSymbolCode,
  fetchHistory, selectRows
} from "./historySlice";
import {Order} from "../../interfaces/app"
import {stableSort, getComparator} from "../../helpers/app"
import {currencies} from "../../helpers/data";

const useStyles = makeStyles({
  table: {
    minWidth: 350,
  },
  inlineFormControl: {
    marginLeft: '2rem',
  }
});

function History(): ReactElement {
  const classes = useStyles();
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [order, setOrder] = React.useState<Order>('asc');
  const lastDays = 7;

  const dispatch = useDispatch();
  const baseCode = useSelector(selectBaseCode)
  const symbolCode = useSelector(selectSymbolCode)
  const rows = useSelector(selectRows)

  const handleOrderChange = (event: React.ChangeEvent<{ name?: string; value: unknown }>) => {
    setOrder(event.target.value as Order);
  };

  const currencyOptions = currencies.map(
    currency => <option value={currency.code} key={currency.code}>{currency.name}</option>
  );

  useEffect( () => {
    const startDate = moment().subtract(lastDays * 2, 'days').format('YYYY-MM-DD');
    const endDate = moment().format('YYYY-MM-DD');
    dispatch(fetchHistory({startDate, endDate, baseCode, symbolCode}));
  }, [baseCode, symbolCode]);

  return (
    <div>
      <div>
        <FormControl>
          <InputLabel htmlFor="base-code">
              Base
          </InputLabel>
          <NativeSelect
            value={baseCode}
            onChange={e => dispatch(setBaseCode(e.target.value))}
            inputProps={{
              'aria-label': 'base-currency',
              name: 'baseCode',
              id: 'base-currency',
            }}
          >
            {currencyOptions}
          </NativeSelect>
        </FormControl>
        <FormControl className={classes.inlineFormControl}>
          <InputLabel htmlFor="select-code">
            Symbol
          </InputLabel>
          <NativeSelect
            value={symbolCode}
            onChange={e => dispatch(setSymbolCode(e.target.value))}
            inputProps={{
              'aria-label': 'select-currency',
              name: 'selectCode',
              id: 'select-code',
            }}
          >
            {currencyOptions}
          </NativeSelect>
        </FormControl>
        <FormControl className={classes.inlineFormControl}>
          <InputLabel htmlFor="order">
            Order Date
          </InputLabel>
          <NativeSelect
            value={order}
            onChange={handleOrderChange}
            inputProps={{
              'aria-label': 'order',
              name: 'order',
              id: 'order',
            }}
          >
            <option value='asc'>Ascending</option>
            <option value='desc'>Descending</option>
          </NativeSelect>
        </FormControl>
      </div>
      <div>
        <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Date</TableCell>
        <TableCell align="right">Rate</TableCell>
        </TableRow>
        </TableHead>
        <TableBody>
        {stableSort<{date: string, rate: number}>(rows, getComparator(order, 'date'))
          .map((row) => (
            <TableRow key={row.date}>
              <TableCell component="th" scope="row">
                {row.date}
              </TableCell>
              <TableCell align="right">
                {row.rate}
              </TableCell>
            </TableRow>
    ))}
      </TableBody>
      </Table>
      </TableContainer>
      </div>
    </div>
  );
}

export default History;