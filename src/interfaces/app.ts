export type Rate = {
  date: string,
  baseCode: string,
  symbolCode: string,
  rate: number,
}

export type Currency = {
  name: string,
  code: string,
}

export type Order = 'asc' | 'desc';